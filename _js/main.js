//Create Canvas 
var canvas = document.createElement('canvas');
var ctx = canvas.getContext('2d');
canvas.width = 480;
canvas.height = 800;
document.body.appendChild(canvas);

//Keyboard Controls 
var keysDown = {};

addEventListener('keydown',function(e){
	keysDown[e.keyCode] = true;
},false);

addEventListener('keyup',function(e){
	delete keysDown[e.keyCode];
},false);

//game objects 

//BLOCK CREATOR
function Block (x1,y1,x2,y2,x3,y3,x4,y4,color){
	this.speed = 1.50;
	this.color = color;
	this.draw = function(x,y){
		ctx.fillStyle = color;
		ctx.fillRect(x+x1,y+y1,40,40);
			this.block1.x = x+x1;
			this.block1.y = y+y1;

		ctx.fillRect(x+x2,y+y2,40,40);
			this.block2.x = x+x2;
			this.block2.y = y+y2;

		ctx.fillRect(x+x3,y+y3,40,40);
			this.block3.x = x+x3;
			this.block3.y = y+y3;

		ctx.fillRect(x+x4,y+y4,40,40);
			this.block4.x = x+x4;
			this.block4.y = y+y4;	
	};

	this.clearBlocks = function(x,y){
		ctx.clearRect(x+x1,y+y1-(1.4+this.speed),40,40);

		ctx.clearRect(x+x2,y+y2-(1.4+this.speed),40,40);

		ctx.clearRect(x+x3,y+y3-(1.4+this.speed),40,40);

		ctx.clearRect(x+x4,y+y4-(1.4+this.speed),40,40);
	};

	this.block1 = {
		width:40,
		height:40,
		id:'block1' + "-" + counter,
		hasCollided: false
	};
	this.block2 = {
		width:40,
		height:40,
		id:'block2' + "-" + counter,
		hasCollided: false
	};
	this.block3 = {
		width:40,
		height:40,
		id:'block3' + "-" + counter,
		hasCollided: false
	};
	this.block4 = {
		width:40,
		height:40,
		id:'block4' + "-" + counter,
		hasCollided: false
	};
}

var collision = function(blocks){
	if(blocks[counter].block1.y >= 760 || blocks[counter].block2.y >= 760 || blocks[counter].block3.y >= 760 || blocks[counter].block4.y >= 760){
		return true;
	} 
};

var endGame = function(blocks){
	if(blocks[counter].block1.y <= 0 || blocks[counter].block2.y <= 0 || blocks[counter].block3.y <= 0 || blocks[counter].block4.y <= 0){
		console.log('ended');
		return true;
	}
};

var collides= function(a, b) {
  return a.x < b.x + b.width &&
         a.x + a.width > b.x &&
         a.y < b.y + b.height &&
         a.y + a.height > b.y;
};

var blockCollision = function(blocks,list){
	for (var i = 0; i<blocks.length; i++){
		for(var a = 0; a<blocks.length; a++){
			if( collides( blocks[a].block1, blocks[i].block2 ) && !(blocks[a].block1.hasCollided === true && blocks[i].block2.hasCollided === true) ){ //block 1 vs block 2
					blocks[a].block1.hasCollided = true;
					blocks[i].block2.hasCollided = true;
					return true;	
			} else if( collides( blocks[a].block1, blocks[i].block3 ) && !(blocks[a].block1.hasCollided === true && blocks[i].block3.hasCollided === true)){ //block 1 vs block 3
					blocks[a].block1.hasCollided = true;
					blocks[i].block3.hasCollided = true;
					return true;
			} else if(collides( blocks[a].block1, blocks[i].block4 ) && !(blocks[a].block1.hasCollided === true && blocks[i].block4.hasCollided === true)){ // block 1 vs block 4
					blocks[a].block1.hasCollided = true;
					blocks[i].block4.hasCollided = true;
					return true;
			} else if( collides( blocks[a].block2, blocks[i].block3 ) && !(blocks[a].block2.hasCollided === true && blocks[i].block3.hasCollided === true) ){ //block 2 vs block 3
					blocks[a].block2.hasCollided = true;
					blocks[i].block3.hasCollided = true;
					return true;
			} else if(collides( blocks[a].block2, blocks[i].block4 ) && !(blocks[a].block2.hasCollided === true && blocks[i].block4.hasCollided === true)){ //block 2 vs block 4
					blocks[a].block2.hasCollided = true;
					blocks[i].block4.hasCollided = true;
					return true;
			} else if(collides( blocks[a].block3, blocks[i].block4 ) && !(blocks[a].block3.hasCollided === true && blocks[i].block4.hasCollided === true)){ //block 3 vs block 4
					blocks[a].block3.hasCollided = true;
					blocks[i].block4.hasCollided = true;
					return true;
			} 
		}
	}
};

var newBlock = function(){
	counter++;

	var randomNumber = Math.floor(Math.random()*7);
	switch(randomNumber){
		case 0: 
			blocks.push(new Block(0,0, 0,40, 0,80, 0,120,'red'));
			break;
		case 1:
			blocks.push(new Block(0,0, -40,40, 0,40, 40,40,'purple'));
			break;
		case 2:
			blocks.push(new Block(0,0, 0,40, 0,80, 40,80,'green'));
			break;
		case 3:
			blocks.push(new Block(0,0, 0,40 ,0,80, -40,80,'blue'));
			break;
		case 4:
			blocks.push(new Block(0,0, 40,0, 0,40, 40,40,'orange'));
			break;
		case 5:
			blocks.push(new Block(0,0, 40,40, 40,0, 80,0,'yellow'));
			break;
		case 6:
			blocks.push(new Block(0,0, 40,0, 40,40, 80,40,'pink'));
			break;
	}

	updateVar = 0;
};

//variables

var blocks = [];
var ignoreList = [];
var updateVar = 0;
var counter = 0;

//New Block (Block Examples) (X1,Y1, X2,Y2, X3,Y3, X4,Y4)
var lineBlock = new Block(0,0, 0,40, 0,80, 0,120,'red');

var tBlock = new Block(0,0, -40,40, 0,40, 40,40,'purple');

var lBlock = new Block(0,0, 0,40, 0,80, 40,80,'green');

var jBlock = new Block(40,0, 40,40 ,40,80, 0,80,'blue');

var squareBlock = new Block(0,0, 40,0, 0,40, 40,40,'orange');

var sBlock = new Block(0,0, 40,40, 40,0, 80,0,'yellow');

var zBlock = new Block(0,0, 40,0, 40,40, 80,40,'pink');



var update = function(modifier){
	updateVar = blocks[counter].speed + updateVar;

	if(endGame(blocks)){
		return true;
	}
	else if(collision(blocks) || blockCollision(blocks,ignoreList) ){
		newBlock();
	} else{

		if(40 in keysDown){ //down
			blocks[counter].clearBlocks(0,updateVar);
			blocks[counter].draw(0,updateVar);	
		}

		if(37 in keysDown){ //left
			console.log('left');
			
			blocks[counter].clearBlocks(-updateVar,0);
			blocks[counter].draw(-updateVar,0);	
		}

		if(39 in keysDown){ //right
			console.log('right');
			blocks[counter].clearBlocks(updateVar,updateVar);
			blocks[counter].draw(updateVar,updateVar);	
		}
		//blocks[counter].block1.y = 40 + blocks[counter].block1.y;
			blocks[counter].clearBlocks(0,updateVar);
			blocks[counter].draw(0,updateVar);		
	}


};

//main game loop 
var main = function(){
	var now = Date.now();
	var delta = now - then;
	update(delta / 1000);

	then = now;

	//request to do again ASAP 
	requestAnimationFrame(main);
};
console.log(zBlock);
var then = Date.now();
blocks.push(new Block(0,80, 40,80, 40,40, 80,40,'red'));
main();